$(document).ready(function() {
    $("#search").click(function() {
        var ketikan = $('#keyword').val(); 
        $.ajax({
            url: "/data?q=" + ketikan,
            success: function(data) {
                console.log(data);
                $("#hasil").empty();
                var items = data.items;

                for (i = 0; i < items.length; i++) {
                    var judul = items[i].volumeInfo.title;
                    var gambar;
                    var link = items[i].volumeInfo.infoLink;
                    

                    try {
                        gambar = items[i].volumeInfo.imageLinks.thumbnail;
                    }
                    catch(err) {
                        try {
                            gambar = items[i].volumeInfo.imageLinks.smallThumbnail;
                        }
                        catch(err) {
                            gambar = gambar_not_found;
                        }
                    }

                    $("#hasil").append("<div class=\"col center buku\"> \
                                        <a href=\"" + link + "\"> \
                                        <img class=\"thumbnail\" src=\"" + gambar +"\"> \
                                        <p style=\"color: white;\">" + judul + "</p> \
                                        </a></div>");
                }
            }
        });
    });
});
