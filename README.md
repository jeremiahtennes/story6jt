# story6jt
Status Pipelines :<br>
[![pipeline status](https://gitlab.com/jeremiahtennes/story6jt/badges/master/pipeline.svg)](https://gitlab.com/jeremiahtennes/story6jt/commits/master)
<br>
<br>
Status Code Coverage :<br>
[![coverage report](https://gitlab.com/jeremiahtennes/story6jt/badges/master/coverage.svg)](https://gitlab.com/jeremiahtennes/story6jt/-/commits/master)
<br>
<br>
Link website :<br>
https://story6jt.herokuapp.com
<br>
