from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .apps import Story7Config
from .views import story7

# Create your tests here.
class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(Story7Config.name, 'story_7')
		self.assertEqual(apps.get_app_config('story_7').name, 'story_7')

class Story7UnitTest(TestCase):
	def test_story7_url_exists(self):
		response = Client().get('/story7/')
		self.assertEqual(response.status_code, 200)
