from django.shortcuts import render

# Create your views here.
def story8(request):
	return render(request, 'story8.html')

def data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    return JsonResponse(ret.json(), safe=False)