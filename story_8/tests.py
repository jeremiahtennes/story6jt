from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .apps import Story8Config
from .views import story8

# Create your tests here.
class TestApp(TestCase):
	def test_apps(self):
		self.assertEqual(Story7Config.name, 'story_8')
		self.assertEqual(apps.get_app_config('story_8').name, 'story_8')

class Story8UnitTest(TestCase):
	def test_story8_url_exists(self):
		response = Client().get('/story8/')
		self.assertEqual(response.status_code, 200)
